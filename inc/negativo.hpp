#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include "imagem.hpp"
#include "filtro.hpp"

class FiltroNegativo : public Filtro{
private:

public:
    FiltroNegativo(){};
    ~FiltroNegativo(){};

    void aplicarFiltro(Imagem &imagem);
};

#endif
