#ifndef MEDIA_HPP
#define MEDIA_HPP

#include "filtro.hpp"

class FiltroMedia : public Filtro{
private:

public:
    FiltroMedia(){};
    ~FiltroMedia(){};

    void copiarHeader(Imagem &imagem, int tamanhoFiltro);
    void aplicarFiltro(Imagem &imagem);
};

#endif
