#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <fstream>
#include "imagem.hpp"

using namespace std;

/* struct representando os pixels da imagem, com cada componente de cor */
typedef struct {
    unsigned char r, g, b;
}colors;

class Filtro{
protected:
    ifstream in;
    ofstream out;
public:
    Filtro(){};
    ~Filtro(){};

    /* método para escrever o cabeçalho no arquivo de saída */
    void copiarHeader(Imagem &imagem);
    
    /* posiciona o arquivo após o cabeçalho, para manipulação dos pixels */
    void setPosition(streampos &pos);

    /* tornando a classe abstrata */
    virtual void aplicarFiltro(Imagem &imagem)=0;
};

#endif
