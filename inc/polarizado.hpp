#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include "imagem.hpp"
#include "filtro.hpp"

class FiltroPolarizado : public Filtro{
private:

public:
    FiltroPolarizado(){};
    ~FiltroPolarizado(){};

    void aplicarFiltro(Imagem &imagem);
};

#endif
