#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <fstream>

using namespace std;

class Imagem{
private:
    ifstream in;
    string nomeArquivo, tipo;
    int largura, altura, max;
    Imagem();
public:
    /* Construtor/Destrutor */
    Imagem(string nome);
    ~Imagem();

    /* Acessores */
    string getTipo();
    void setTipo(string tipo);

    int getLargura();
    void setLargura(int largura);

    int getAltura();
    void setAltura(int altura);

    int getMax();
    void setMax(int max);

    string getNomeArquivo();
    void setNomeArquivo(string nomeArquivo);

    /* Outros métodos */
    void abrirArquivo(string nomeArquivo, ifstream &arquivo);
    void fecharArquivo();

    void extrairDados();
};

#endif
