#ifndef GRAYSCALE_HPP
#define GRAYSCALE_HPP

#include "imagem.hpp"
#include "filtro.hpp"

class FiltroGrayscale : public Filtro{
private:

public:
    FiltroGrayscale(){};
    ~FiltroGrayscale(){};

    void aplicarFiltro(Imagem &imagem);
};

#endif
