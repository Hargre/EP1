#include <iostream>
#include <fstream>
#include <string>

#include "grayscale.hpp"

using namespace std;

void FiltroGrayscale::aplicarFiltro(Imagem &imagem){
    colors pix;
    colors new_pix;
    /* gerando arquivo de saída para a imagem filtrada */
    string caminho = string("doc/") + imagem.getNomeArquivo();
    in.open(caminho.c_str());

    string copia = caminho + string("(grayscale)");
    out.open(copia.c_str());

    this->copiarHeader(imagem);
    /* --------------------------------------------------------- */

    /* posicionando o arquivo de entrada para leitura dos pixels */
    streampos pos;
    this->setPosition(pos);
    /* --------------------------------------------------------- */

    /* aplicando o filtro */
    for(int i = 0; i < imagem.getLargura(); ++i){
        for(int j = 0; j < imagem.getAltura(); ++j){
            in.read((char*)&pix.r, 1);
            in.read((char*)&pix.g, 1);
            in.read((char*)&pix.b, 1);

            int grayValue = (0.299 * pix.r) + (0.587 * pix.g) + (0.144 * pix.b);

            new_pix.r = grayValue;
            new_pix.g = grayValue;
            new_pix.b = grayValue;

            out.write((char*)&new_pix.r, 1);
            out.write((char*)&new_pix.g, 1);
            out.write((char*)&new_pix.b, 1);
        }
    }
    out.close();
}
