#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>

#include "imagem.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "grayscale.hpp"
#include "media.hpp"

using namespace std;

int main(){
    /* objeto criado como ponteiro para poder ser usado em blocos try/catch */
    Imagem *original;
    string nome;
    int selecao;

    cout << "Diga o nome do arquivo a ser aberto:" << endl;
    cin >> nome;

    /* valida existência da imagem e seu formato */
    try{
        original = new Imagem(nome);
    }
    catch(runtime_error &e){
        cout << e.what() << endl;
        return 1;
    }

    /* menu */
    cout << "Selecione operação a ser realizada com a imagem:" << endl;
    cout << "------------------------------------------------" << endl;
    cout << "1 - Filtro negativo" << endl;
    cout << "2 - Filtro polarizado" << endl;
    cout << "3 - Filtro preto e branco" << endl;
    cout << "4 - Filtro de média" << endl;

    do {
        cin >> selecao;
        switch(selecao){
            case 1:{
                FiltroNegativo negativo;
                negativo.aplicarFiltro(*original);
                break;
            }
            case 2:{
                FiltroPolarizado polarizado;
                polarizado.aplicarFiltro(*original);
                break;
            }
            case 3:{
                FiltroGrayscale gray;
                gray.aplicarFiltro(*original);
                break;
            }
            case 4:{
                FiltroMedia media;
                media.aplicarFiltro(*original);
                break;

            }
            default:{
                cout << "Entrada inválida! Favor inserir novamente: " << endl;
                break;
            }
        }
    } while(selecao < 1 || selecao > 4);

    /* desalocando memória */
    delete (original);
    return 0;
}
