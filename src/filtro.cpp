#include "filtro.hpp"

/* método para escrever o cabeçalho no arquivo de saída */
void Filtro::copiarHeader(Imagem &imagem){
    out << imagem.getTipo() << endl;
    out << imagem.getLargura() << " ";
    out << imagem.getAltura() << endl;
    out << imagem.getMax() << endl;
}

/* posiciona o arquivo após o cabeçalho, para manipulação dos pixels */
void Filtro::setPosition(streampos &pos){
    string tipo, comentario;
    int largura, altura, max;
    getline(in, tipo);
    do{
        pos = in.tellg();
        getline(in, comentario);
        if(comentario[0] != '#'){
            in.seekg(pos);
        }
    }while(comentario[0]=='#');

    in >> largura;
    in >> altura;
    in >> max;

    in.seekg(1, in.cur);
    pos = in.tellg();
}
