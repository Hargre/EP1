#include <iostream>
#include <fstream>
#include <string>

#include "negativo.hpp"

using namespace std;

void FiltroNegativo::aplicarFiltro(Imagem &imagem){
    colors pix;
    colors new_pix;
    /* gerando arquivo de saída para a imagem filtrada */
    string caminho = string("doc/") + imagem.getNomeArquivo();
    in.open(caminho.c_str());

    string copia = caminho + string("(negativo)");
    out.open(copia.c_str());

    this->copiarHeader(imagem);
    /* --------------------------------------------------------- */

    /* posicionando o arquivo de entrada para leitura dos pixels */
    streampos pos;
    this->setPosition(pos);
    /* --------------------------------------------------------- */

    /* aplicando o filtro */
    for(int i = 0; i < imagem.getLargura(); ++i){
        for(int j = 0; j < imagem.getAltura(); ++j){
            in.read((char*)&pix.r, 1);
            in.read((char*)&pix.g, 1);
            in.read((char*)&pix.b, 1);

            new_pix.r = imagem.getMax() - pix.r;
            new_pix.g = imagem.getMax() - pix.g;
            new_pix.b = imagem.getMax() - pix.b;

            out.write((char*)&new_pix.r, 1);
            out.write((char*)&new_pix.g, 1);
            out.write((char*)&new_pix.b, 1);
        }
    }
    out.close();
}
