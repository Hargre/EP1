#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "media.hpp"

using namespace std;

void FiltroMedia::copiarHeader(Imagem &imagem, int tamanhoFiltro){
    int borda = tamanhoFiltro - 1;
    out << imagem.getTipo() << endl;
    out << imagem.getLargura()-borda << " ";
    out << imagem.getAltura()-borda << endl;
    out << imagem.getMax() << endl;
}

void FiltroMedia::aplicarFiltro(Imagem &imagem){
    int tamanhoFiltro, selecao;

    cout << "Selecione tamanho do filtro:" << endl;
    cout << "--------------------------- " << endl;
    cout << "1 - 3x3" << endl;
    cout << "2 - 5x5" << endl;
    cout << "3 - 7x7" << endl;

    do{
        cin >> selecao;
        switch(selecao) {
            case 1:{
                tamanhoFiltro = 3;
                break;
            }
            case 2:{
                tamanhoFiltro = 5;
                break;
            }
            case 3:{
                tamanhoFiltro = 7;
                break;
            }
            default:{
                cout << "Tamanho inválido! Favor inserir novamente: " << endl;
                break;
            }
        }
    }while(selecao < 1 || selecao > 3);

    /* alocando a matriz */
    colors **pixels = new colors*[imagem.getAltura()];
    for(int i=0; i < imagem.getAltura(); i++){
        pixels[i] = new colors[imagem.getLargura()];
    }

    /* abrindo arquivos */
    string caminho = string("doc/") + imagem.getNomeArquivo();
    in.open(caminho.c_str());

    stringstream tamanho;
    tamanho << tamanhoFiltro;
    string str = tamanho.str();

    string copia = caminho + string("(") + str + string("x") + str + string(")");
    out.open(copia.c_str());

    /* escrevendo o header na imagem filtrada */
    this->copiarHeader(imagem, tamanhoFiltro);

    /* posicionando o arquivo de entrada para leitura dos pixels */
    streampos pos;
    this->setPosition(pos);
    /* --------------------------------------------------------- */

    /* copiando os dados para a matriz */
    for(int i = 0; i < imagem.getAltura(); ++i){
        for(int j = 0; j < imagem.getLargura(); ++j){
            in.read((char*)&pixels[i][j].r, 1);
            in.read((char*)&pixels[i][j].g, 1);
            in.read((char*)&pixels[i][j].b, 1);
        }
    }

    int limite = tamanhoFiltro/2;
    int div = tamanhoFiltro*tamanhoFiltro;
    /* aplicando o filtro */
    for(int i = limite; i < imagem.getAltura()-limite; ++i){
        for(int j = limite; j < imagem.getLargura()-limite; ++j){

            int r = 0;
            int g = 0;
            int b = 0;

            for(int x = i-limite; x <= i+limite; ++x){
                for(int y = j-limite; y <= j+limite; ++y){
                    r += pixels[x][y].r;
                    g += pixels[x][y].g;
                    b += pixels[x][y].b;
                }
            }

            r = r/div;
            g = g/div;
            b = b/div;

            out.write((char*)&r, 1);
            out.write((char*)&g, 1);
            out.write((char*)&b, 1);
        }
    }
    out.close();

    /* desalocando a memória */
    for(int i = 0; i < imagem.getAltura(); i++){
        delete[] pixels[i];
    }
    delete[] pixels;
}
