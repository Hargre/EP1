#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>

#include "imagem.hpp"

using namespace std;

/* construtor já realiza abertura de arquivo e leitura do header */
Imagem::Imagem(string nome){
    this->abrirArquivo(nome, in);
    this->extrairDados();
}

Imagem::~Imagem(){}

/* get/set para tipo */
string Imagem::getTipo(){
    return tipo;
}
void Imagem::setTipo(string tipo){
    this->tipo = tipo;
}

/* get/set para largura */
int Imagem::getLargura(){
    return largura;
}
void Imagem::setLargura(int largura){
    this->largura = largura;
}

/* get/set para altura */
int Imagem::getAltura(){
    return altura;
}
void Imagem::setAltura(int altura){
    this->altura = altura;
}

/* get/set para max */
int Imagem::getMax(){
    return max;
}
void Imagem::setMax(int max){
    this->max = max;
}

/* get/set para nomeArquivo */
string Imagem::getNomeArquivo(){
    return nomeArquivo;
}
void Imagem::setNomeArquivo(string nomeArquivo){
    this->nomeArquivo = nomeArquivo;
}

/* métodos para abrir/fechar arquivos */
void Imagem::abrirArquivo(string nomeArquivo, ifstream &arquivo){
    this->setNomeArquivo(nomeArquivo);
    string caminho = string("doc/") + nomeArquivo;
    arquivo.open(caminho.c_str());
    if(!arquivo){
        throw runtime_error("Arquivo não existe!");
    }
}

void Imagem::fecharArquivo(){
    in.close();
}

/* obtém as informações do header */
void Imagem::extrairDados(){
    streampos pos;
    string tipo, comentario;
    int largura, altura, max;

    /* lendo tipo, verificando validade do formato */
    getline(in, tipo);
    if(tipo != "P6"){
        throw runtime_error("Formato inválido!");
    }
    this->setTipo(tipo);
    cout << "Tipo: " << this->getTipo() << endl;

    /* ignorando comentários */
    do{
        pos = in.tellg();
        getline(in, comentario);
        if(comentario[0] != '#'){
            in.seekg(pos);
        }
    } while(comentario[0]=='#');

    /* lendo dimensões e escala de cores */
    in >> largura;
    this->setLargura(largura);
    cout << "Largura: " << this->getLargura() << endl;

    in >> altura;
    this->setAltura(altura);
    cout << "Altura: " << this->getAltura() << endl;

    in >> max;
    this->setMax(max);
    cout << "Escala máxima de cores: " << this->getMax() << endl;

    this->fecharArquivo();
}
