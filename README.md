# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Instruções para compilação e execução do programa (em Linux): 

### Para compilar:
* Abrir o terminal;
* Mudar para o diretório raiz do projeto;
* Digitar **$ make clean** para limpar arquivos objetos já existentes
* Compilar com **$ make**
* Executar com **$ make run**

### Para executar: 
* Todas as imagens devem estar localizadas na pasta doc, presente no diretório raiz.
* Deve-se fornecer apenas o nome da imagem, com sua extensão .ppm ao final.
* Após indicar a imagem, se a mesma existir e for válida, deve-se selecionar qual filtro deseja ser aplicado.
* O menu possui 4 opções, sendo estas:
	1. Filtro negativo;
	2. Filtro polarizado;
	3. Filtro preto e branco;
	4. Filtro de média.
* Se a opção escolhida for o Filtro de média, será apresentado ao usuário um novo menu, para escolher o tamanho da máscara a se aplicar.
* As opções nesse caso são:
	1. 3x3;
	2. 5x5;
	3. 7x7.
* As imagens filtradas serão salvas na mesma pasta doc, e terão o nome igual ao da respectiva imagem original, acrescido do filtro aplicado entre parentêses.
* Exemplo:
	* Imagem original: unb.ppm
	* Imagem gerada após se aplicar o filtro negativo: unb.ppm(negativo)
